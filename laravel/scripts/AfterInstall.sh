#!/bin/bash
sudo cp -R /var/www/friday-webapp/laravel/.env.example /var/www/friday-webapp/laravel/.env
cd /var/www/friday-webapp/laravel
sudo composer install
sudo chown -R www-data:www-data /var/www/friday-webapp
chmod 777 /var/www/friday-webapp/laravel/storage
chmod 777 /var/www/friday-webapp/laravel/bootstrap/cache
sudo php artisan key:generate
